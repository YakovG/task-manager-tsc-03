# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

NAME: Yakov Goloshchapov

E-MAIL: ygoloshchapov@tsconsulting.com

# SOFTWARE

* JDK 1.8

* Windows 10

# HARDWARE

* RAM 16Gb

* CPU i5

* HDD 128Gb

# RUN PROGRAM

***
java -jar task-manager.jar
***

# SCREENSHOT

https://yadi.sk/d/wU25TrDKjEC-eg?w=1